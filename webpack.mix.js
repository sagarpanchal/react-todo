const mx = require("laravel-mix");

if (!mx.inProduction()) {
  mx.webpackConfig({ devtool: "source-map" }).sourceMaps();
}

mx
  .options({ processCssUrls: false })
  .setPublicPath("public/assets")
  .react("src/react/index.js", "public/assets/js")
  .sass("src/sass/app.scss", "public/assets/css");
