import React, { Component } from "react";
import { notify } from "react-notify-toast";
import ToDo from "./components/ToDo";
import "./libraries/FontAwesome";

export default class App extends Component {
  constructor() {
    super();
    this.notify = React.createRef();

    if (localStorage.getItem("state") !== null) {
      let state = { ...JSON.parse(localStorage.getItem("state")) };
      this.state.list = state.list;
    }

    window.onfocus = function() {
      let state = { ...JSON.parse(localStorage.getItem("state")) };
      if (state.list !== undefined) {
        this.setState({ list: state.list });
      }
    }.bind(this);
  }

  state = {
    list: [],
    input: {
      type: "default",
      text: ""
    },
    show: "all"
  };

  toastColors = {
    error: {
      color: "#fff",
      backgroundColor: "#ff0039"
    },
    success: {
      color: "#fff",
      backgroundColor: "#3fb618"
    },
    warning: {
      color: "#fff",
      backgroundColor: "#ff7518"
    },
    info: {
      color: "#fff",
      backgroundColor: "#373a3c"
    }
  };

  clearField = () => {
    let input = { ...this.state.input };
    input.text = "";
    this.setState({ input });
  };

  inputChanged = e => {
    let input = { ...this.state.input };
    input.text = e.target.value;
    this.setState({ input }, function() {
      localStorage.setItem("state", JSON.stringify(this.state));
    });
  };

  typeChanged = e => {
    let input = { ...this.state.input };
    input.type = e.target.value.toLowerCase();
    this.setState({ input }, function() {
      localStorage.setItem("state", JSON.stringify(this.state));
    });
  };

  inputKeyPressed = e => {
    if (e.key === "Enter") {
      this.addItem();
    }
  };

  addItem = () => {
    let { text, type } = this.state.input;
    if (text.trim() === "") {
      notify.show("Please enter text", "info", 2000);
      return false;
    }
    let { list } = this.state;
    let duplicate = false;
    list.map(
      listItem => (listItem.text === text.trim() ? (duplicate = true) : null)
    );
    if (duplicate) {
      notify.show("Duplicate entries aren't allowed", "info", 2000);
      return false;
    }
    this.clearField();
    list.push({ text: text.trim(), type: type.toLowerCase(), done: false });
    this.setState({ list }, function() {
      localStorage.setItem("state", JSON.stringify(this.state));
      notify.show("Added: " + text, "success", 1500);
    });
  };

  deleteListItem = index => {
    let { list } = this.state;
    list.splice(index, 1);
    this.setState({ list }, function() {
      localStorage.setItem("state", JSON.stringify(this.state));
      notify.show("Deleted", "info", 2000);
    });
  };

  toggleListItem = index => {
    let { list } = this.state;
    list[index].done ? (list[index].done = false) : (list[index].done = true);
    this.setState({ list }, function() {
      localStorage.setItem("state", JSON.stringify(this.state));
    });
  };

  filterListItems = type => {
    this.setState({ show: type });
  };

  getClassList = index => {
    let { list, show } = this.state;
    let classes = "todo-entry border-radius-0";
    if (
      (!list[index].done && show === "completed") ||
      (list[index].done && show === "due")
    ) {
      return classes + " d-none";
    }
    return classes;
  };

  render() {
    return (
      <ToDo
        types={[
          "Default",
          "Primary",
          "Secondary",
          "Success",
          "Info",
          "Warning",
          "Danger"
        ]}
        toastColors={this.toastColors}
        states={["All", "Due", "Completed"]}
        list={this.state.list}
        input={this.state.input}
        show={this.state.show}
        inputChanged={this.inputChanged}
        inputKeyPressed={this.inputKeyPressed}
        typeChanged={this.typeChanged}
        addItem={this.addItem}
        getClassList={this.getClassList}
        toggleListItem={this.toggleListItem}
        deleteListItem={this.deleteListItem}
        filterListItems={this.filterListItems}
      />
    );
  }
}
