import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Button,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  Input,
  ListGroup,
  ListGroupItem
} from "reactstrap";
import Notifications from "react-notify-toast";

const ToDo = ({
  types,
  toastColors,
  states,
  list,
  input,
  show,
  inputChanged,
  inputKeyPressed,
  typeChanged,
  addItem,
  getClassList,
  toggleListItem,
  deleteListItem,
  filterListItems
}) => {
  return (
    <div className="container-fluid p-3">
      <Notifications options={{ colors: toastColors }} />
      <h2 className="text-center">Todo</h2>
      <div className="d-flex flex-row justify-content-center my-3">
        <InputGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
          {/* Input */}
          <Input
            type="text"
            placeholder="Add Task"
            value={input.text}
            onChange={inputChanged}
            onKeyPress={inputKeyPressed}
            className="border-radius-0"
          />
          <InputGroupAddon addonType="append">
            {/* select */}
            <select className="custom-select" onChange={typeChanged}>
              {types.map((option, index) => (
                <option key={index}>{option}</option>
              ))}
            </select>
          </InputGroupAddon>
          <InputGroupAddon addonType="append">
            {/* Button */}
            <Button
              color="secondary"
              onClick={addItem}
              className="border-radius-0"
            >
              <FontAwesomeIcon icon="plus" />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </div>
      {/* Button Group */}
      <div className="d-flex flex-row justify-content-center mb-3">
        <ButtonGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
          {states.map((option, index) => (
            <Button
              onClick={() => filterListItems(option.toLowerCase())}
              key={index}
              outline={show !== option.toLowerCase() ? true : null}
              className="col-lg-4"
            >
              {option}
            </Button>
          ))}
        </ButtonGroup>
      </div>
      {/* Todo List */}
      <div className="d-flex flex-row justify-content-center">
        <ListGroup className="col-md-8 col-lg-5 col-xl-4 p-0" id="todo-list">
          {list.map((listItem, index) => (
            <ListGroupItem
              color={listItem.type}
              key={index}
              className={getClassList(index)}
            >
              <span
                className={
                  "icon-check " + (!listItem.done ? "icon-muted" : null)
                }
              >
                <FontAwesomeIcon icon="check-square" />
              </span>
              <div
                className="check-item"
                onClick={() => toggleListItem(index)}
              />
              <div
                className={
                  "list-item-text" +
                  (listItem.done ? " list-item-done text-muted" : "")
                }
              >
                {listItem.text}
              </div>
              <span className="icon-delete">
                <FontAwesomeIcon icon="trash" />
              </span>
              <div
                className="delete-item"
                onClick={() => deleteListItem(index)}
              />
            </ListGroupItem>
          ))}
        </ListGroup>
      </div>
      {/* ListItem Preview */}
      {input.text !== "" ? (
        <div className="d-flex flex-row justify-content-center mt-1">
          <ListGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
            <ListGroupItem
              color={input.type}
              className="todo-entry border-secondary border-radius-0 animated slideInUp faster"
            >
              {input.text}
            </ListGroupItem>
          </ListGroup>
        </div>
      ) : null}
    </div>
  );
};

ToDo.propTypes = {
  types: PropTypes.array,
  toastColors: PropTypes.object,
  states: PropTypes.array,
  list: PropTypes.array,
  input: PropTypes.object,
  show: PropTypes.string,
  inputChanged: PropTypes.func,
  inputKeyPressed: PropTypes.func,
  typeChanged: PropTypes.func,
  addItem: PropTypes.func,
  getClassList: PropTypes.func,
  toggleListItem: PropTypes.func,
  deleteListItem: PropTypes.func,
  filterListItems: PropTypes.func
};

export default ToDo;
