import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { faTrash } from "@fortawesome/free-solid-svg-icons/faTrash";
import { faCheckSquare } from "@fortawesome/free-solid-svg-icons/faCheckSquare";

library.add(faPlus, faTrash, faCheckSquare);
