[![Netlify Status](https://api.netlify.com/api/v1/badges/55df869c-e745-4b20-a3fc-b63e3c8a4b5e/deploy-status)](https://app.netlify.com/sites/wizardly-fermat-e3c70e/deploys)

# Setup Instructions

```
npm i   # to install required npm packages
```

### ESLint plugins for vscode

```
npm i -g eslint eslint-plugin-react babel-eslint
```

### NPM scripts

```
npm run watch   # for live updates while testing,
npm run dev     # for development build and
npm run prod    # for production build.
```

### Delete .git in windows

```
cmd> del /F /S /Q /A .git
```
